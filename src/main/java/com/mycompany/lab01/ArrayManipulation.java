package com.mycompany.lab01;

public class ArrayManipulation {

    public static void main(String[] args) {
        //no.2
        int[] numbers = {5, 8, 3, 2, 7};
        String[] names = {"Alice", "Bob", "Charlie", "David"};
        double[] values = new double[4];
        
        //no.3 print elements of numbers
        for(int i=0; i<numbers.length; i++) {
            System.out.print(numbers[i] + " ");
        }System.out.println("");
        
        //no.4 print elements of names use for each
        for(String name : names) {
            System.out.print(name + " ");
        }System.out.println("");
        
        //no.5 initialize value
        values[0] = 0.5;
        values[1] = 1.5;
        values[2] = 2.3;
        values[3] = 4.4;
        
        int sum = 0;
        //no.6 calculate and sum of all elements numbers
        for(int i=0; i<numbers.length; i++) {
            sum += numbers[i];
        }System.out.println("sum of all elements in numbers array = "+sum);
        
        //no.7 find max values in values
        double max = 0;
        for(int i=0; i<values.length; i++) {
            if(max<values[i]) {
                max = values[i];
            }
        }System.out.println("Maximum values in values array = " + max);
        
        String[] reversedNames = new String[4];
        
        for (int i = 0; i < names.length; i++) {
            reversedNames[(names.length-1) - i] = names[i];
        }
        for(int i =0; i < names.length; i++){
            System.out.print(reversedNames[i]+ " ");
        }
    }
}
